import firebase from 'firebase';

export const initializeFirebase = async () => {
  try {
    firebase.initializeApp({
      messagingSenderId: '643019687510'
    });

    // A service worker must already be registered.
    navigator.serviceWorker.ready.then(registration => {
      const messaging = firebase.messaging();
      messaging.useServiceWorker(registration);
    });
  } catch(err) {
    console.error(err);
  }
}

export const askForPermission = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    console.log('Permission granted');

    const token = await messaging.getToken();
    console.log('Token:', token);
    
    return token;
  } catch (err) {
    console.log('Permission not granted');
    console.error(err);
  }
}

export const sendTokenToServer = async (token) => {
  try {
    
  } catch(err) {
    console.log(err);
  }
}