import React, { Component } from 'react';
import * as pushNotification from '../../push-notification';
import './Layout.css';

class Layout extends Component {
  constructor(opts) {
    super(opts);

    this.state = {
      subscribe: true,
      unsubscribe: false
    }
  }

  subscribeHandler = async () => {
    // Request permission.
    // Returns a token generated per service worker.
    let token = await pushNotification.askForPermission();

    // Send token to server to manage subscription.
    await pushNotification.sendTokenToServer(token);

    this.setState({
      subscribe: false,
      unsubscribe: true
    });
  }

  unsubscribeHandler = () => {
    this.setState({
      subscribe: true,
      unsubscribe: false
    });
  }

  render() {
    return (
      <div>
        <div className='heading'>
          <h1>Getting started with Firebase Push Notifications</h1>
        </div>

        <div className='btnGroup'>
          <button 
          id='subscribe'
          className={ this.state.subscribe ? 'selected':'' }
          onClick={ () => this.subscribeHandler() }>Subscribe</button>

          <button 
          id='unsubscribe'
          className={ this.state.unsubscribe ? 'selected':'' }
          onClick={ () => this.unsubscribeHandler() }>Unsubscribe</button>
        </div>
      </div>
    );
  }
}

export default Layout;