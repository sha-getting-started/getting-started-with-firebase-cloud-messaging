import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './containers/Layout/Layout';
import * as serviceWorker from './service-worker';
import * as pushNotification from './push-notification';

ReactDOM.render(<Layout />, document.getElementById('root'));
serviceWorker.register();
pushNotification.initializeFirebase();