importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.0.0/workbox-sw.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');


self.addEventListener('install', (event) => {
  // Automatically skip waiting and update service worker.
  self.skipWaiting();
});

self.addEventListener('notificationclick', async (event) => {
  event.notification.close();

  let openWindow = async () => {
    let clientsList = await clients.matchAll({
      url: 'http://localhost:3000',
      visibilityState: 'hidden',
      type: 'window'
    })

    if(clientsList[0]) {
      return clientsList[0].focus();
    }

    clients.openWindow('http://localhost:3000');
  }

  event.waitUntil(openWindow());
});


firebase.initializeApp({
  messagingSenderId: '643019687510'
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler((message) => {
  // The message must contain a 'data' property.
  // If 'notification' property is used in place of 'data'
  // then the message options will override and the defaults
  // specified won't be invoked.
  let title = message.data.title;
  let options = {
    body: message.data.body,
    icon: message.data.icon || './assets/icons/firebase.png',
    tag: message.data.tag  // Used to group notifications
  }

  return self.registration.showNotification(title, options);
});


workbox.routing.registerRoute('http://localhost:3000/', 
  new workbox.strategies.NetworkFirst());

workbox.routing.registerRoute(/\.(?:js|css|html|ico|png|jpeg)$/, 
  new workbox.strategies.NetworkFirst());